import React, { Component } from 'react'
import SocialButton from './SocialButton.jsx'


export default class SocialLogin extends Component {

  onLoginSuccess = (user) => {
    console.log(user)
    // window.open('/home','_self')
  }

  onLoginFailure = (err) => {
    console.error(err)
  }

  onLogoutSuccess = () => {
    console.log('inside onLogoutSuccess')
  }

  onLogoutFailure = (err) => {
    console.error(err)
  }

  logout = () => {
    console.log('inside logout')
    // const { logged, currentProvider } = this.state

    // if (logged && currentProvider) {
    //   this.nodes[currentProvider].props.triggerLogout()
    // }
  }
  
  render () {
    let children

      children = [
        <SocialButton
          provider='facebook'
          appId='1532846240185799'
          onLoginSuccess={this.onLoginSuccess}
          onLoginFailure={this.onLoginFailure}
          onLogoutSuccess={this.onLogoutSuccess}
          key={'facebook'}
        >
          <i className={"fab fa-facebook"} />
        </SocialButton>,
        <SocialButton
          provider='google'
          appId='656786888058-j0aal867rfb6m39l0b8156dhbjmp02tm.apps.googleusercontent.com'
          onLoginSuccess={this.onLoginSuccess}
          onLoginFailure={this.onLoginFailure}
          onLogoutSuccess={this.onLogoutSuccess}
          onLogoutFailure={this.onLogoutFailure}
          key={'google'}
        >
          <i className={"fab fa-google-plus-g"} />
        </SocialButton>
      ]
    return children
  }

}