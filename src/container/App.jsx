import React,{Component} from 'react'
import store from '../../src/store'
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router'
import { createBrowserHistory } from 'history'
import LoginPage from '../views/LoginPage/LoginPage';
import LandingPage from '../views/LandingPage/LandingPage';



class App extends Component {
   
  render(){ 
      return ( 
            <Provider store={store}>
                <Router history={createBrowserHistory()} >
                   <Switch >
                        <Route exact path="/" component={LoginPage}/>
                        <Route exact path="/home" component={LandingPage}/>
                   </Switch>
                </Router>                      
            </Provider>
        )
}
}

export default (App);