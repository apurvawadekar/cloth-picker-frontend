import React from 'react';
import ReactDOM from 'react-dom';
import "./assets/scss/material-kit-react.scss?v=1.7.0"
import App from '../src/container/App.jsx';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
