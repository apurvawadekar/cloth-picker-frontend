import React, { Component } from 'react'

//Redux store 
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

// nodejs library to set properties for components
import PropTypes from 'prop-types'
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
// core components
import Header from '../../components/Header/Header'
import HeaderLinks from '../../components/Header/HeaderLinks'
import Footer from '../../components/Footer/Footer'
import GridContainer from '../../components/Grid/GridContainer'
import GridItem from '../../components/Grid/GridItem'
import Card from '../../components/Card/Card'
import CardHeader from '../../components/Card/CardHeader'

import loginPageStyle from '../../assets/jss/material-kit-react/views/loginPage.jsx'

import image from '../../assets/img/bg8.png'
import SocialLogin from '../../components/SocialLogin/SocialLogin.jsx'

class LoginPage extends Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }


  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          absolute
          color="transparent"
          brand=""
          rightLinks={<HeaderLinks />}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                <Card className={classes[this.state.cardAnimaton]}>
                  <form className={classes.form}>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <h4>Login | Sign Up</h4>
                      <div className={classes.socialLine}>
                        <SocialLogin />
                      </div>
                    </CardHeader>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
          <Footer whiteFont />
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object
};

// const mapStateToProps = state => ({
//   selectedCountry: state.country.selectedCountry,
//   rates: state.currency.rates
// });


// const mapDispatchToProps = dispatch => ({
//   countryActions: bindActionCreators(countryActions, dispatch),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

export default withStyles(loginPageStyle)(LoginPage);
