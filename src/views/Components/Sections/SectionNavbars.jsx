import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

// @material-ui/icons
// import Email from "@material-ui/icons/Email";

// core components
import Header from "../../../components/Header/Header.jsx";
import CustomDropdown from "../../../components/CustomDropdown/CustomDropdown.jsx";
import Button from "../../../components/CustomButtons/Button.jsx";

import navbarsStyle from "../../../assets/jss/material-kit-react/views/componentsSections/navbarsStyle.jsx";
import profileImage from "../../../assets/img/faces/avatar.jpg";

class SectionNavbars extends React.Component {
  render() {
    const { classes } = this.props;
    return (
        <div className={classes.container}>
          <div id="navbar" className={classes.navbar}>
              <Header
                fixed
                brand="Cloth Picker Web App"
                color="info"
                rightLinks={
                  <List className={classes.list}>
                    <ListItem className={classes.listItem}>
                      <Button
                        href="#pablo"
                        className={classes.navLink}
                        onClick={e => e.preventDefault()}
                        color="transparent"
                      >
                        My Collection
                      </Button>
                    </ListItem>
                    <ListItem className={classes.listItem}>
                      <Button
                        href="#pablo"
                        className={classes.navLink}
                        onClick={e => e.preventDefault()}
                        color="transparent"
                      >
                        Bookmarked
                      </Button>
                    </ListItem>
                    {/* <ListItem className={classes.listItem}>
                      <Button
                        justIcon
                        round
                        href="#pablo"
                        className={classes.notificationNavLink}
                        onClick={e => e.preventDefault()}
                        color="rose"
                      >
                        <Email className={classes.icons} />
                      </Button>
                    </ListItem> */}
                    <ListItem className={classes.listItem}>
                      <CustomDropdown
                        left
                        caret={false}
                        hoverColor="black"
                        dropdownHeader="My Account"
                        buttonText={
                          <img
                            src={profileImage}
                            className={classes.img}
                            alt="profile"
                          />
                        }
                        buttonProps={{
                          className:
                            classes.navLink + " " + classes.imageDropdownButton,
                          color: "transparent"
                        }}
                        dropdownList={[
                          "Me",
                          "Settings and other stuff",
                          "Sign out"
                        ]}
                      />
                    </ListItem>
                  </List>
                }
              />
          </div>
        </div>
    );
  }
}

SectionNavbars.propTypes = {
  classes: PropTypes.object
};

export default withStyles(navbarsStyle)(SectionNavbars);
